package com.example.epark;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeCheck extends AppCompatActivity {
    SpaceLot spaceLot = new SpaceLot();
    Button start, end, receipt;
    TextView In, Out, ParkingText;
    String[] Field = new String[1];
    String[] Data = new String[1];
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_check);
        start = (Button) findViewById(R.id.btn_start);
        end = (Button) findViewById(R.id.btn_end);
        receipt = (Button) findViewById(R.id.btn_continue);
        In = (TextView) findViewById(R.id.Time_In);
        Out = (TextView) findViewById(R.id.Time_Out);
        ParkingText = (TextView) findViewById(R.id.Park_Number);
        Intent i = getIntent();
        ParkingText.setText(i.getStringExtra("ParkNo"));
        Field[0] = "ticket";
        Data[0] = i.getStringExtra("CODE2");
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currTime = new SimpleDateFormat("h:mm a", Locale.getDefault()).format(new Date());
                In.setText(currTime);
                PutData sender = new PutData("http://192.168.1.20/ePark/v1/timeIn.php", "POST", Field, Data);
                if (sender.startPut()) {
                    if (sender.onComplete()) {
                        result = sender.getResult();
                        if(result.equals("Update Complete")){
                            start.setVisibility(View.INVISIBLE);
                            end.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currTime = new SimpleDateFormat("h:mm a", Locale.getDefault()).format(new Date());
                Out.setText(currTime);
                PutData sender = new PutData("http://192.168.1.20/ePark/v1/timeout.php", "POST", Field, Data);
                if (sender.startPut()) {
                    if (sender.onComplete()) {
                        result = sender.getResult();
                        if(result.equals("Update Complete")){
                            end.setVisibility(View.INVISIBLE);
                            receipt.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });
        receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}