package com.example.epark;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class SpaceLot extends AppCompatActivity {
    String item;
    Spinner spinnerParking;
    ArrayList<String> parkingList = new ArrayList<>();
    ArrayAdapter<String> parkingAdapter;
    RequestQueue requestQueue;
    Button btn_continue;
    TextView Debug;
    String[] Field = new String[2];
    String[] Data = new String[2];
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_lot);
        btn_continue = findViewById(R.id.button2);
        requestQueue = Volley.newRequestQueue(this);
        spinnerParking = (Spinner) findViewById(R.id.spinnerPark);
        Debug = (TextView) findViewById(R.id.Debug_2);
        Intent i = getIntent();
        String CODE = i.getStringExtra("CODE1");
        Field[0] = "ticket";
        Field[1] = "park";
        Data[0] = i.getStringExtra("CODE1");
        parkingList.add("");
        String url = "http://192.168.1.20/epark/v1/parkSpace.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("park");
                    for (int v = 0; v < jsonArray.length(); v++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(v);
                        String parking = jsonObject.optString("parkno");
                        parkingList.add(parking);
                        parkingAdapter = new ArrayAdapter<>(SpaceLot.this, android.R.layout.simple_spinner_item, parkingList);
                        parkingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerParking.setAdapter(parkingAdapter);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest);
        spinnerParking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item =(String.valueOf(parent.getItemAtPosition(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent launch = new Intent(SpaceLot.this, TimeCheck.class);
                    launch.putExtra("CODE2",CODE);
                    launch.putExtra("ParkNo", item);
                    startActivity(launch);
                    finish();
                }

        });




    }
    public String getItem(){
        return item;
    }
    public void setItem(String d){
        this.item = d;
    }
}