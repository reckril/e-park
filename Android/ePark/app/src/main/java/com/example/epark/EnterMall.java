package com.example.epark;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

import java.util.Random;

public class EnterMall extends AppCompatActivity {
    TextView mall,parkingpay,code1,code2,code3,code4,code5,code6, message, ticketm, Debug;
    Button btn_continue1, btn_enter;
    String coding="";
    Random rnd = new Random();
    String[] codearray = new String[6];
    String ticketno;
    String[] Field = new String[1];
    String[] Data = new String[1];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_mall);
        mall = (TextView) findViewById(R.id.textView2);
        parkingpay = (TextView) findViewById(R.id.textView);
        message = (TextView) findViewById(R.id.message);
        ticketm = (TextView) findViewById(R.id.textView5);
        Debug = (TextView) findViewById(R.id.DEBUG_TEXT);
        code1 = (TextView) findViewById(R.id.Code1);
        code2 = (TextView) findViewById(R.id.Code2);
        code3 = (TextView) findViewById(R.id.Code3);
        code4 = (TextView) findViewById(R.id.Code4);
        code5 = (TextView) findViewById(R.id.Code5);
        code6 = (TextView) findViewById(R.id.Code6);
        btn_enter = (Button) findViewById(R.id.btn_Mall);
        btn_continue1 = (Button) findViewById(R.id.btn_Continue1);
        Field[0] = "ticket";
        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_enter.setVisibility(View.INVISIBLE);
                mall.setVisibility(View.INVISIBLE);
                btn_enter.setVisibility(View.INVISIBLE);
                parkingpay.setVisibility(View.INVISIBLE);
                ticketm.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                code1.setVisibility(View.VISIBLE);
                code2.setVisibility(View.VISIBLE);
                code3.setVisibility(View.VISIBLE);
                code4.setVisibility(View.VISIBLE);
                code5.setVisibility(View.VISIBLE);
                code6.setVisibility(View.VISIBLE);
                btn_continue1.setVisibility(View.VISIBLE);
                Code();
                code1.setText(codearray[0]);
                code2.setText(codearray[1]);
                code3.setText(codearray[2]);
                code4.setText(codearray[3]);
                code5.setText(codearray[4]);
                code6.setText(codearray[5]);
            }
        });

        btn_continue1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result="";
                PutData sender = new PutData("http://192.168.1.20/ePark/v1/registerUser.php", "POST", Field, Data);
                if (sender.startPut()) {
                    if (sender.onComplete()) {
                        result = sender.getResult();
                    }
                }
                if(result.equals("User Created Successfully")){
                    Intent launch = new Intent(EnterMall.this, SpaceLot.class);
                    launch.putExtra("CODE1",coding);
                    startActivity(launch);
                    finish();
                }
            }
        });
    }

    public void Code(){
        String result1 = "";
        Boolean test = true;
        while(test){
            coding = "";
            for(int x=0;x<6;x++) {
                int y = (int) rnd.nextInt(10);
                coding = coding + y;
                codearray[x] = String.valueOf(y);
            }
            ticketno = String.valueOf(coding);
            Data[0] = ticketno;
            PutData sender = new PutData("http://192.168.1.20/ePark/v1/searchData.php", "POST", Field, Data);
            if (sender.startPut()) {
                if (sender.onComplete()) {
                    result1 = sender.getResult();
                    if (result1.equals("User Found")){
                    }else{
                        test = false;
                        break;
                    }
                }
            }
        }

    }
}