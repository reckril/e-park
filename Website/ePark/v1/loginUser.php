<?php

    require_once '../DBOperations.php';

    $response = array();

    if($_SERVER['REQUEST_METHOD']=='POST'){

        if(isset($_POST['ticket'])){
            $db = new DBOperation();

            if($db->userLogin($_POST['ticket'])){
              $ticket = $db->getuserByusername($_POST['ticket']);
              $response['error'] = false;
              $response['message'] = "Successful Login";
              $response['id'] = $ticket['id'];
              $response['ticket'] = $ticket['ticketno'];
            }else{
                $response['error'] = true;
                $response['message'] = "Invalid ticket";
            }
        }else{
            $response['error'] = true;
            $response['message'] = "Missing Fields Detected";
        }
    }else{
        $response['error'] = true;
        $response['message'] = "Server Issue";
    }

    echo json_encode($response);