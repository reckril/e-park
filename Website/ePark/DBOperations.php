<?php

    class DbOperation{
        private $con;

        function __construct(){

            require_once dirname(__FILE__).'/DBConnect.php';

            $db = new DbConnect();

            $this->con = $db->connect();
        }

        public function createUser($username){
            //$password =  md5($pass);
            $stmt = $this->con->prepare("INSERT INTO tbl_ticket (ticketno, date) VALUES (?, CURRENT_DATE)");						
            $stmt->bind_param("s",$username);
            if	($stmt->execute())	{
                return true;
            }else{																
                return false;
            }
        }

        public function getAvail(){
            $stmt = $this->con->prepare("SELECT * FROM tbl_parking WHERE available = 'Available'");
            $stmt->execute();
            $result=$stmt->get_result();
            return $result;
        }


        public function userLogin($username){
            //$password = md5($pass);
            $stmt = $this->con->prepare("SELECT id FROM tbl_ticket WHERE ticketno = ?");
            //$stmt->bind_param("ss",$username,$password);
            $stmt->bind_param("s",$username);
            $stmt->execute();
            $stmt->store_result();
            return $stmt->num_rows > 0;
        }

        public function getUserByUsername($username){
            $stmt = $this->con->prepare("SELECT * FROM tbl_ticket WHERE ticketno = ?");
            $stmt->bind_param("s",$username);
            $stmt->execute();
            return $stmt->get_result()->fetch_assoc();
        }

        public function getByParkNo($username){
            $stmt = $this->con->prepare("SELECT * FROM tbl_parking WHERE parkno = ?");
            $stmt->bind_param("s",$username);
            $stmt->execute();
            return $stmt->get_result()->fetch_assoc();
        }

        public function getUserParkNo($username){
            $stmt = $this->con->prepare("SELECT parkno FROM tbl_ticket Where ticketno = ?");
            $stmt->bind_param("s",$username);
            $stmt->execute();
            return $stmt->get_result()->fetch_assoc();
        }

        public function timein($id){
            $stmt = $this->con->prepare("UPDATE tbl_ticket SET timein = CURRENT_TIME WHERE id = ?");
            $stmt->bind_param("s",$id);
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }

        }

        public function timeout($id){
            $stmt = $this->con->prepare("UPDATE tbl_ticket SET timeout = CURRENT_TIME WHERE id = ?");
            $stmt->bind_param("s",$id);
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }

        }
        public function parksreg($id,$park){
            $stmt = $this->con->prepare("UPDATE tbl_ticket SET parkno = ? WHERE id = ?");
            $stmt->bind_param("ss",$park,$id);
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }

        }

        public function parksOcc($id){
            $stmt = $this->con->prepare("UPDATE tbl_ticket SET available = 'Occupied' WHERE id = ?");
            $stmt->bind_param("s",$id);
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }

        }
    }
    ?>