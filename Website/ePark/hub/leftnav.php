<style>
    ul{
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    ul li ul li{
        display: none;
    }
    ul li:hover ul li{
        display: block;
    }
    ul li div#navfix a{
        text-decoration: none;
        color: white;
    }
    
    ul li div#navfix{
        height: 3%;
        width: 100%;
        margin-top: 5px;
        padding-top: 5px;
        border-style: solid none none none;
        border-color: white;
        
    }
    div#inclose{
        margin-top: 5%;
    }
    a{ 
        text-decoration: none;
        color: white;
    }
    th#edit{
        text-align: center;
        background-color: #347474;
        border-width: 80%;
        width: 77.5px;
    }

    th#delete{
        text-align: center;
        background-color: #f0134d;
        border-width: 80%;
    }
    .delete{
        background-color: #f0134d;
        border: none;
        color: white;
        padding: 2px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        font-style: bold;
    } 
    p{
        font-style: italic;
    }
    #ml{
        margin-left: 5px;
    }
    #b5 {
	border:2px solid #456879;
	border-radius:10px;
	height: 22px;
    width: 230px;
    }
    div#leftpage{
       width:  20%;
       height: 100%;
       background-color: #a9a9a9;
       margin-top: 0%;
       margin-bottom: 0%;
       margin-left: 0%;

    }
</style>
<div id=leftpage>
    <img src="" alt="EPARK LOGO" id=icons>
    <ul>
    <li><div id="navfix"><a></a></div></li>
    </ul>
</div>